# Cookies API
<br />
We all know cookies are good so why don't you use this program to remember those amazing cooking instructions!

Try the beta by following the instructions below:

Web app is hosted on Heroku at: https://test-project-trent-evans.herokuapp.com/  
  
Start your own list:  
  1. Type your name in the box next to "First Name:" and leave it there for all the following steps  
  2. Click the "Add Cookies from CSV" button to get started. As of now this uses a pre-made  
      CSV file  
  3. Step 2 isn't necessary but kinda cool :)  

  ---------------------  

Create a yummy cookie!:  
  1. Click the "Add Cookie" button  
  2. Fill in the yummy info  
  3. Click the "Add cookie" button at the bottom right of the popup   

---------------------   

Check out ALL of your cookies!:   
  1. Just type the name you used in the first step of "Start your own list" instructions above  

---------------------  

Update an instruction!:  
  1. Click the pencil button next to the cookie you want to edit  
  2. Make changes  
  3. Click the "Update" button at the bottom right of the popup  

---------------------  

Delete an instruction:  
  1. Click the red 'x' button next to the cookie you want to delete  
  2. Click the "Delete" button in the popup  
  *Note: If you change your mind then just click the close button and it won't be deleted*  

Eat real cookie:  
  1. Use one of your instructions you created to bake cookies  
  2. Eat ALL cookies!  
    2.1 Milk is suggested to accompany your delicious cookies :)  

------------------------------------------------------------   
You are tasked with creating a minimal REST API which provides endpoints to enable CRUD operations on cookies . . . no, not browser cookies, actual   cookies (chocolate chip, peanut butter, sugar, etc).  

You will develop the endpoints and methods for the API so a user can manage their cookie (object) stash via HTTP calls. You will also provide   documentation in this README about how to formulate those calls to the endpoints  
so the user can clearly understand how to manage their cookies. Data about the cookie objects will persist in an instance of MongoDB under a database   called, interestingly enough, `cookies`.  

The user should be able to use the API to create new cookies, edit their properties (you get to decide which properties they have), delete (eat) cookies, and get information about all their cookies. For example, how many chocolate chip cookies do I have right now?  

As an example, a `cookie` object could look like this in the db:  

```  
{  
  'user_id': 'dood',  
	'flavor': 'snicker doodle',  
	'temp': 113,  
	'radius': 3.4,  
	'deliciousness': 10,  
	'toppings': false,  
	'date_baked': 10790343458,  
	'fresh': true  
}  
```  

An interesting endpoint might be something like `http://cookieapi.com/top_delicious/10`, which returns the top 10 most delicious cookies belonging to a   user. Your creativity and sky is the limit, but don't spend too much time on it :)  

You will be using Python 3.x with Flask and pymongo. Set up a development environment on your machine, and when you submit the project you can provide us with a dump of the data in your `cookies` db.  

We'll be looking at the following criteria:  

- How intuitive it is to use  
- Object-oriented approach  
- Logical consistency of HTTP methods and their resulting actions and responses  
- Conciseness and clarity  
- Error handling  
- Use of git to manage development flow  
- Extra points for creativity and interesting features  
- Extra points for test cases  

Don't worry about security--authentication, authorization, etc, but a user should have to provide their `user_id` or similar identifier with every call to the API.  

Contact ben.coleman@savantx.com with any questions. Good luck!  
