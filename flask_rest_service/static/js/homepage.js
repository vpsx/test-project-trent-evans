var app = angular.module('DemoApp',[]);

app.controller('DemoController',function($scope, $http){

    $scope.info = {};
    $scope.showCookiesList = function(){
      $http({
        method: 'GET',
        url: '/cookies'
      }).then(function(response) {
        $scope.cookies = response.data;
        console.log('Cookies:',$scope.cookies);
      }, function(error) {
        console.log(error);
      });
    }
    });
