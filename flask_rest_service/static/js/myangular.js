var app = angular.module('DemoApp',[]);

app.controller('DemoController', function($scope, $http){
    $scope.info = {};
    $scope.showAdd = false;
    $scope.firstName = "Public";
    $scope.id = null;
    $scope.file = null;

    $scope.onReset = function(){
      $scope.firstName = "";
    }

    $scope.$watch( 'firstName',
            function(newValue, oldValue){
                console.log('firstName Changed');
                console.log(newValue);
                console.log(oldValue);
                $scope.firstName = newValue;
                $scope.showCookiesList();
            }
    );

    $scope.showCookiesList = function(){
      $http({
        method: 'GET',
        url: '/cookies/'+$scope.firstName
      }).then(function(response) {
        $scope.cookies = response.data;
        console.log('Cookies:',$scope.cookies);
      }, function(error) {
        console.log(error);
      });
    }

    $scope.showCookie = function(){
      $http({
        method: 'GET',
        url: '/cookie/read'+$scope.info.id
      }).then(function(response) {
        $scope.cookie = response.data;
        console.log('Cookie: ',$scope.cookie);
      }, function(error) {
        console.log(error);
      });
    }

    $scope.addCookiesFromCsv = function(){
      $http({
        method: 'GET',
        url: '/csv/'+$scope.firstName,
      }).then(function(response) {
        $scope.showCookiesList();
        $scope.info = {}
      }, function(error) {
        console.log(error);
      });
    }


    $scope.file_changed = function(element) {

         $scope.$apply(function(scope) {
             var photofile = element.files[0];
             $http({
               method: 'POST',
               url: '/csv/'+$scope.firstName,
               data: {file:photofile}
             }).then(function(response) {
               $scope.file = response.data;
               console.log('Cookie: ',$scope.file);
             }, function(error) {
               console.log(error);
             });
         });
    };

    $scope.addCookie = function(){
      $http({
        method: 'POST',
        url: '/cookie/create/'+$scope.firstName,
        data: {info:$scope.info}
      }).then(function(response) {
        $scope.showCookiesList();
        $('#addPopUp').modal('hide')
        $scope.info = {}
      }, function(error) {
        console.log(error);
      });
    }

    $scope.editCookie = function(unique_id){
      $scope.info.id = unique_id;
      $scope.id = unique_id;
      $scope.showAdd = false;

      $http({
        method: 'POST',
        url: '/cookieid/'+unique_id,
        data: {unique_id:$scope.info.id}
      }).then(function(response) {
        console.log(response);
        $scope.info = response.data;
        $scope.showCookie;
        $('#addPopUp').modal('show')
      }, function(error) {
        console.log(error);
      });
    }

    $scope.updateCookie = function(unique_id){
      $http({
        method: 'PUT',
        url: '/cookie/update/'+$scope.id,
        data: {info:$scope.info}
      }).then(function(response) {
        console.log(response.data);
        $scope.showCookiesList();
        $('#addPopUp').modal('hide')
      }, function(error) {
        console.log(error);
      });
    }

    $scope.showAddPopUp = function(){
      $scope.showAdd = true;
      $scope.info = {};
      $('#addPopUp').modal('show')
    }

    $scope.showRunPopUp = function(unique_id){
      $scope.info.id = unique_id;
      $scope.run = {};

      $http({
        method: 'POST',
        url: '/cookie/read/'+unique_id,
        data: {id:$scope.info.id}
      }).then(function(response) {
        console.log(response);
        $scope.run = response.data;
        $scope.run.isRoot = false;
        $('#runPopUp').modal('show');
      }, function(error) {
        console.log(error);
      });
    }

    $scope.confirmDelete = function(unique_id){
      $scope.deleteCookieId = unique_id;
      $('#deleteConfirm').modal('show');
    }

    $scope.deleteCookie = function(){
      $http({
        method: 'DELETE',
        url: '/cookie/delete/'+$scope.deleteCookieId,
        data: {id:$scope.deleteCookieId}
      }).then(function(response) {
        console.log(response.data);
        $scope.deleteCookieId = '';
        $scope.showCookiesList();
        $('#deleteConfirm').modal('hide')
      }, function(error) {
        console.log(error);
      });
    }
  });
