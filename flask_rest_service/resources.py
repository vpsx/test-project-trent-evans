import json, uuid, time, os, csv
import pandas as pd
from json import dumps
from flask import Flask, request, abort, render_template, Response
from flask_restful import Resource, Api, reqparse
from flask_rest_service import app, api, mongo
from flask_cors import CORS, cross_origin
from werkzeug import secure_filename
from bson.objectid import ObjectId

@app.route('/')
def showCookies():
    return render_template('index.html')

class AllCookies(Resource):
    def get(self):
        return mongo.db.cookies.find()

class CookieListByName(Resource):
    def get(self, name):
        return mongo.db.cookies.find({"name":name})

class CookieListByNameDeliciousness(Resource):
    def get(self, name, deliciousness):
        return mongo.db.cookies.find({"name":name, "deliciousness":deliciousness})

class CookieById(Resource):
    def post(self, unique_id):
        return mongo.db.cookies.find_one_or_404({"unique_id":unique_id})


class Cookie(Resource):
    def get(self, unique_id):
        return mongo.db.cookies.find_one_or_404({"unique_id": unique_id})

    def post(self, name):
        json_data = request.json['info']
        unique_id = str(uuid.uuid4())
        flavor = json_data['flavor']
        temperature = json_data['temp']
        radius = json_data['radius']
        deliciousness = json_data['deliciousness']
        toppings = json_data['toppings']
        date_baked = time.strftime("%d/%m/%Y")
        fresh = json_data['fresh']

        cookie_id =  mongo.db.cookies.insert(
            {
                'unique_id':unique_id,
                'name':name,
                'flavor':flavor,
                'temp':temperature,
                'radius':radius,
                'deliciousness':deliciousness,
                'toppings':toppings,
                'date_baked':date_baked,
                'fresh':fresh
            }
        )
        return mongo.db.cookies.find_one({"_id": cookie_id})

    def put(self, unique_id):
                #dat = mongo.db.cookies.find_one({"unique_id":unique_id})
                cookieInfo = request.json['info']
                print(cookieInfo)
                unique_id = unique_id
                name = cookieInfo['name']
                flavor = cookieInfo['flavor']
                temperature = cookieInfo['temp']
                radius = cookieInfo['radius']
                deliciousness = cookieInfo['deliciousness']
                toppings = cookieInfo['toppings']
                date_baked = time.strftime("%d/%m/%Y")
                fresh = cookieInfo['fresh']

                mongo.db.cookies.update_one(
                    {'unique_id':unique_id},
                    {
                        "$set": {
                            'name':name,
                            'flavor':flavor,
                            'temp':temperature,
                            'radius':radius,
                            'deliciousness':deliciousness,
                            'toppings':toppings,
                            'date_baked':date_baked,
                            'fresh':fresh
                        }
                    }
                )
                return mongo.db.cookies.find_one({"unique_id": unique_id})

    def delete(self, unique_id):
        mongo.db.cookies.find_one_or_404({"unique_id": unique_id})
        mongo.db.cookies.remove({"unique_id": unique_id})
        return '', 204

class Csv(Resource):
    def get(self, name):

        with open('cookies.csv', 'w') as csvfile:
            filewriter = csv.writer(csvfile, delimiter=',',
                            quotechar='|', quoting=csv.QUOTE_MINIMAL)
            filewriter.writerow(['flavor','temp','radius','deliciousness','toppings','fresh'])
            filewriter.writerow(['Chocolate','800','3','10','More chocolate!','true'])
            filewriter.writerow(['Mint','900','7','8','Actual mints','true'])

        csvfile.close
        f = pd.read_csv('cookies.csv')
        #dpayload = csv.to_json()
        cookieInfo = json.loads(f.to_json(orient='records'))
        #mongo.db.cookies.insert(payload)
        for x in cookieInfo:
            print(x)
            unique_id = str(uuid.uuid4())
            name = name
            flavor = x['flavor']
            temperature = x['temp']
            radius = x['radius']
            deliciousness = x['deliciousness']
            toppings = x['toppings']
            date_baked = time.strftime("%d/%m/%Y")
            fresh = x['fresh']

            cookie_id =  mongo.db.cookies.insert(
                {
                    'unique_id':unique_id,
                    'name':name,
                    'flavor':flavor,
                    'temp':temperature,
                    'radius':radius,
                    'deliciousness':deliciousness,
                    'toppings':toppings,
                    'date_baked':date_baked,
                    'fresh':fresh
                }
            )

        return mongo.db.cookies.find({"name":name})
        #csv = pd.read_csv()




routes = [
    '/cookie/create/<string:name>',
    '/cookie/read/<string:unique_id>',
    '/cookie/delete/<string:unique_id>',
    '/cookie/update/<string:unique_id>'
]

#api.add_resource(Root, '/')
api.add_resource(AllCookies, '/cookies')
api.add_resource(CookieListByName, '/cookies/<string:name>')
api.add_resource(CookieListByNameDeliciousness, '/cookies/<string:name>/<int:deliciousness>')
api.add_resource(Cookie, *routes)
api.add_resource(Csv, '/csv/<string:name>')
api.add_resource(CookieById, '/cookieid/<string:unique_id>')
