from flask import Flask, request, jsonify
from flask_pymongo import PyMongo
import time, os

MONGO_URL = os.environ.get('MONGO_URL')
if not MONGO_URL:
    MONGO_URL = 'mongodb://localhost:27017/deliciousdb';

app = Flask(__name__)

# Will connect to mongodb running on localhost
app.config['MONGO_URI'] = MONGO_URL

mongo = PyMongo(app)
cookie = mongo.db.cookies

# app routes with accompanying methods go here

@app.route('/cookie', methods=['GET'])
def get_all_cookies():
    output = []
    for s in cookie.find():
        output.append({'user_id' : s['user_id'], 'flavor' : s['flavor'], 'temp' : s['temp'],
        'radius' : s['radius'], 'deliciousness' : s['deliciousness'], 'toppings' : s['toppings'],
        'date_baked' : s['date_baked'], 'fresh' : s['fresh']})
    return jsonify({'result' : output})

@app.route('/cookie/<user_id>', methods=['GET'])
def get_users_cookies(user_id):
    user = cookie.find({"user_id" : user_id})
    output = []
    for c in user:
        output.append({'user_id' : c['user_id'], 'flavor' : c['flavor'], 'temp' : c['temp'],
        'radius' : c['radius'], 'deliciousness' : c['deliciousness'], 'toppings' : c['toppings'],
        'date_baked' : c['date_baked'], 'fresh' : c['fresh']})
    return jsonify({'result' : output})

@app.route('/cookie/<user_id>', methods=['POST'])
def add_cookie(user_id):
    flavor = request.json['flavor']
    temperature = request.json['temp']
    radius = request.json['radius']
    deliciousness = request.json['deliciousness']
    toppings = request.json['toppings']
    date_baked = time.strftime("%d/%m/%Y")
    fresh = request.json['fresh']

    bake_cookie = cookie.insert(
        {
            'user_id':user_id,
            'flavor':flavor,
            'temp':temperature,
            'radius':radius,
            'deliciousness':deliciousness,
            'toppings':toppings,
            'date_baked':date_baked,
            'fresh':fresh
        }
    )

    new_cookie = cookie.find_one(
        {
            '_id':bake_cookie
        }
    )

    output = {
        'name':new_cookie['user_id'],
        'flavor':new_cookie['flavor'],
        'temp':new_cookie['temp'],
        'radius':new_cookie['radius'],
        'deliciousness':new_cookie['deliciousness'],
        'toppings':new_cookie['toppings'],
        'date_baked':new_cookie['date_baked'],
        'fresh':new_cookie['fresh']
    }

    return jsonify(
        {
            'result':output
        }
    )

if __name__ == '__main__':
    app.run(debug = True)
